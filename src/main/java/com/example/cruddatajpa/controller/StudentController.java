package com.example.cruddatajpa.controller;

import com.example.cruddatajpa.model.Student;
import com.example.cruddatajpa.model.StudentPage;
import com.example.cruddatajpa.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/findAll")
    @ResponseBody
    public List<Student> getAllStudent(){
        return studentService.getStudent();
    }

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }

    @GetMapping("/findAllWithPaging")
   public ResponseEntity<Page<Student>> findStudents (StudentPage studentPage){
        return new ResponseEntity<>(studentService.getStudents(studentPage), HttpStatus.OK);
    }

    @GetMapping("/findOne/{id}")
    public Student findOne(@PathVariable Long id){
        return studentService.findOne(id);
    }

    @PutMapping("/updateStu/{id}")
    public Student updateStu(@PathVariable Long id,@RequestBody Student student){
        return studentService.updateStudent(id,student);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        studentService.deleteStudent(id);
        return "Successful";
    }
}
