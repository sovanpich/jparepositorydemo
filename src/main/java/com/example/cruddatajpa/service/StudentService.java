package com.example.cruddatajpa.service;
import com.example.cruddatajpa.model.Student;
import com.example.cruddatajpa.model.StudentPage;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    List<Student> getStudent();
    Student addStudent(Student student);
    void deleteStudent(Long id);
    Page<Student> getStudents(StudentPage studentPage);
    Student findOne(Long id);
    Student updateStudent(Long id,Student student);

}
