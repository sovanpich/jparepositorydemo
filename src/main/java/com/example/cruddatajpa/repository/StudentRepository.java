package com.example.cruddatajpa.repository;

import com.example.cruddatajpa.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> , PagingAndSortingRepository<Student,Long> {
    @Query("SELECT stu  FROM Student as stu where id=?1")
    Student findByOne(Long id);

}
