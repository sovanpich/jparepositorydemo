package com.example.cruddatajpa.service.Imp;

import com.example.cruddatajpa.model.Student;
import com.example.cruddatajpa.model.StudentPage;
import com.example.cruddatajpa.repository.StudentRepository;
import com.example.cruddatajpa.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImp implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getStudent() {
        return studentRepository.findAll();
    }

    @Override
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Page<Student> getStudents(StudentPage studentPage) {
        Sort sort = Sort.by(studentPage.getSortDirection(),studentPage.getSortBy());
        Pageable pageable = PageRequest.of(studentPage.getPageNumber(),studentPage.getPageSize(),sort);
        return studentRepository.findAll(pageable);
    }

    @Override
    public Student findOne(Long id) {
        return studentRepository.findByOne(id);
    }

    @Override
    public Student updateStudent(Long id ,Student student) {
        Student stuUpdate = findOne(id);
        stuUpdate.setId(id);
        stuUpdate.setName(student.getName());
        stuUpdate.setAge(student.getAge());
        stuUpdate.setRoom(student.getRoom());
        return studentRepository.save(stuUpdate);
    }
}
